import java.util.ArrayList;

/**
 * Created by Mengjie
 * Date : 2017/12/16
 * Time : 16:21
 **/
public class GameRunner {
    boolean stopGame = false;
    boolean isHard = false;
    boolean isManual = false;

    public void start() {
        //user choose to enter guesses manually, or to automatically guess based on pre-supplied guesses in a file
        while (true) {
            System.out.println("Enter guesses manually, press 1.");
            System.out.println("Automatically guess based on pre-supplied guesses in a file, press 2: ");
            String modeIndex = Keyboard.readInput();
            if (modeIndex.equals("1")) {
                isManual = true;
            }else if (modeIndex.equals("2")) {
                break;
            }else{
                continue;
            }
        }

        //user choose easy mode or hard mode
        while (true) {
            System.out.printf("Easy mode press 1, Hard mode press 2: ");
            String modeIndex = Keyboard.readInput();
            if (modeIndex.equals("1")) {
                break;
            } else if (modeIndex.equals("2")) {
                isHard = true;
                break;
            } else {
                continue;
            }
        }

        //get secret code from user
        //if user input is empty, more or less than 4 digits, having duplicate digit, non-digit in input
        //catch certain exceptions
        UserCodeGenerator userCodeGenerator = new UserCodeGenerator();
        ArrayList<Integer> userSecretCode = userCodeGenerator.secretCode();

        //randomly create a secret code for computer
        CodeGenerator codeGenerator = new CodeGenerator();
        ArrayList<Integer> comSecretCode = codeGenerator.random();
        System.out.println(comSecretCode);

        //start to guess
        int tryCount = 1;
        //user choose to play easy
        if (!isHard) {
            while (tryCount <= 7) {
                //user guess
                UserGuess userGuess = new UserGuess();
                userGuess.guess(comSecretCode);
                //user win and quit the game
                if (userGuess.getUserResult()) {
                    break;
                }
                //computer guess
                EasyComGuess easyComGuess = new EasyComGuess();
                easyComGuess.guess(userSecretCode);
                //computer win and quit the game
                if (easyComGuess.getEasyComResult()) {
                    break;
                }
                tryCount++;
            }
            //user choose to play hard
        } else {
            HardComGuess hardComGuess = new HardComGuess();
            ArrayList<ArrayList<Integer>> possibleGuess = hardComGuess.initialGuess();
            int comBull = 0;
            int comCow = 0;

            while (tryCount <= 7) {
                //user guess
                UserGuess userGuess = new UserGuess();
                userGuess.guess(comSecretCode);
                //user win and quit the game
                if (userGuess.getUserResult()) {
                    break;
                }

                //computer guess
                ArrayList<Integer> guess = hardComGuess.newGuess(possibleGuess);
                hardComGuess.guess(userSecretCode, guess);
                if (hardComGuess.getHardComResult()) {
                    break;
                }

                comBull = hardComGuess.getBull();
                comCow = hardComGuess.getCow();
                hardComGuess.elimination(possibleGuess, guess, comBull, comCow);

                tryCount++;
            }
        }

        //reach maximum try count, result is "Draw"
        if (tryCount == 7) {
            System.out.println("Draw!");
        }

        while (!stopGame) {
            System.out.printf("Do you want play again? (Y/N): ");
            String choice = Keyboard.readInput();
            if (choice.toLowerCase().equals("y")) {
                break;
            } else if (choice.toLowerCase().equals("n")) {
                stopGame = true;
            } else {
                continue;
            }
        }

    }

    public boolean playAgain() {
        return !stopGame;
    }

    public static void main(String[] args) {
        GameRunner g = new GameRunner();
        while (g.playAgain()) {
            g.start();
        }
    }
}
