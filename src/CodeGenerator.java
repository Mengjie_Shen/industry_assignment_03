import java.util.ArrayList;

/**
 * Created by Mengjie
 * Date : 2017/12/16
 * Time : 18:27
 **/
public class CodeGenerator {
    ArrayList<Integer> comCode = new ArrayList<>();

    //get a random integer
    //if integer doesn't exist in intList, add into inList
    //otherwise, get a new random integer
    public ArrayList<Integer> random() {

        while (comCode.size() < 4) {
            int randomInt = (int) (Math.random() * 10);
            if (!comCode.contains(randomInt)) {
                comCode.add(randomInt);
            }

        }

        return comCode;
    }

    @Override
    public String toString() {
        String outStr = "";
        for (int i : comCode) {
            outStr += String.valueOf(i);
        }
        return outStr;
    }
}
