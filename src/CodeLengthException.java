/**
 * Created by Mengjie
 * Date : 2017/12/16
 * Time : 18:46
 **/
public class CodeLengthException extends Exception {
    public CodeLengthException(String s) {
        super(s);
    }
}
