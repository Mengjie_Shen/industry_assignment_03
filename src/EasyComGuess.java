import java.util.ArrayList;

/**
 * Created by Mengjie
 * Date : 2017/12/17
 * Time : 12:26
 **/
public class EasyComGuess {
    boolean comWin = false;

    public void guess(ArrayList<Integer> userCode) {

        CodeGenerator codeGenerator = new CodeGenerator();
        ArrayList<Integer> comGuess = codeGenerator.random();
        System.out.println("Computer guess: " + codeGenerator.toString());

        GameProcessor gameProcessor = new GameProcessor(userCode, comGuess);
        System.out.println(gameProcessor.toString());
        System.out.println("---");
        if (gameProcessor.getBull() == 4) {
            System.out.println("Computer win! :)");
            comWin = true;
        }
    }

    public boolean getEasyComResult() {
        return comWin;
    }
}
