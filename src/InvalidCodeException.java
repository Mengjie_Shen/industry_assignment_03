/**
 * Created by Mengjie
 * Date : 2017/12/16
 * Time : 18:36
 **/
public class InvalidCodeException extends Exception {
    public InvalidCodeException(String s) {
        super(s);
    }
}
