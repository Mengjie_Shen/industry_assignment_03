import java.util.ArrayList;

/**
 * Created by Mengjie
 * Date : 2017/12/17
 * Time : 13:42
 **/
public class UserGuess {
    public boolean userWin = false;

    public void guess(ArrayList<Integer> comCode) {
        boolean isGuessValid = false;
        ArrayList<Integer> userGuess = new ArrayList<>();
        while (!isGuessValid) {

            System.out.printf("You guess: ");
            try {
                UserCodeGenerator guessCode = new UserCodeGenerator();
                userGuess = guessCode.getUsersInput();
                isGuessValid = guessCode.isValid(userGuess);
            } catch (InvalidCodeException e) {
                System.out.println(e.getMessage());
            } catch (EmptyInputException e) {
                System.out.println(e.getMessage());
            } catch (CodeLengthException e) {
                System.out.println(e.getMessage());
            } catch (DuplicateDigitException e) {
                System.out.println(e.getMessage());
            }
        }

        GameProcessor gameProcessor = new GameProcessor(comCode, userGuess);
        System.out.println(gameProcessor.toString());
        System.out.println("");
        if (gameProcessor.getBull() == 4) {
            System.out.println("You win! :)");
            userWin = true;
        }
    }

    public boolean getUserResult() {
        return userWin;
    }
}
