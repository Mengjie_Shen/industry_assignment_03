import java.util.ArrayList;
import java.util.Iterator;


/**
 * Created by Mengjie
 * Date : 2017/12/17
 * Time : 12:27
 **/
public class HardComGuess {
    boolean comWin = false;
    int bull = 0;
    int cow = 0;

    public ArrayList<ArrayList<Integer>> initialGuess() {
        ArrayList<ArrayList<Integer>> possibleGuess = new ArrayList<>();
        for (int i = 0; i <= 9; i++) {
            for (int j = 0; j <= 9; j++) {
                for (int k = 0; k <= 9; k++) {
                    for (int l = 0; l <= 9; l++) {
                        ArrayList<Integer> value = new ArrayList<>();
                        boolean duplicate = i == j || i == k || i == l || j == k || j == l || k == l;
                        if (!duplicate) {
                            value.add(i);
                            value.add(j);
                            value.add(k);
                            value.add(l);
                            possibleGuess.add(value);
                        }
                    }
                }
            }
        }
        return possibleGuess;
    }

    public void elimination(ArrayList<ArrayList<Integer>> possibleGuess, ArrayList<Integer> lastGuess, int bull, int cow) {
        Iterator<ArrayList<Integer>> iterator = possibleGuess.iterator();
        while (iterator.hasNext()) {
            ArrayList<Integer> guess = iterator.next();
            GameProcessor gameProcessor = new GameProcessor(lastGuess, guess);
            if (gameProcessor.getBull() != bull || gameProcessor.getCow() != cow) {
                iterator.remove();
            }
        }
    }

    public ArrayList<Integer> newGuess(ArrayList<ArrayList<Integer>> possibleGuess) {
        int index = (int) (Math.random() * possibleGuess.size());
        return possibleGuess.get(index);
    }

    public void guess(ArrayList<Integer> userCode, ArrayList<Integer> comGuess) {

        System.out.println("Computer guess: " + comGuess.toString());

        GameProcessor gameProcessor = new GameProcessor(userCode, comGuess);
        System.out.println(gameProcessor.toString());
        System.out.println("---");
        if (gameProcessor.getBull() == 4) {
            System.out.println("Computer win! :)");
            comWin = true;
        }
        bull = gameProcessor.getBull();
        cow = gameProcessor.getCow();
    }

    public boolean getHardComResult() {
        return comWin;
    }

    public int getBull() {
        return bull;
    }

    public int getCow() {
        return cow;
    }


}
