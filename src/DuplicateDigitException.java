/**
 * Created by Mengjie
 * Date : 2017/12/16
 * Time : 20:26
 **/
public class DuplicateDigitException extends Exception {
    public DuplicateDigitException(String s) {
        super(s);
    }
}
