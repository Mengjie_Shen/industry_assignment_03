import java.util.ArrayList;

/**
 * Created by Mengjie
 * Date : 2017/12/17
 * Time : 12:55
 **/
public class UserCodeGenerator {

    public ArrayList<Integer> secretCode() {
        ArrayList<Integer> secretCode = new ArrayList<>();
        boolean isSecretCodeValid = false;
        while (!isSecretCodeValid) {
            System.out.printf("Please enter your secret code: ");
            try {
                secretCode = getUsersInput();
                isSecretCodeValid = isValid(secretCode);
            } catch (InvalidCodeException e) {
                System.out.println(e.getMessage());
            } catch (CodeLengthException e) {
                System.out.println(e.getMessage());
            } catch (EmptyInputException e) {
                System.out.println(e.getMessage());
            } catch (DuplicateDigitException e) {
                System.out.println(e.getMessage());
            }
        }
        return secretCode;
    }

    public ArrayList<Integer> getUsersInput() throws InvalidCodeException {
        String inputStr = Keyboard.readInput();
        ArrayList<Integer> userCode = new ArrayList<>();
        for (int i = 0; i < inputStr.length(); i++) {
            try {
                int digit = Integer.parseInt(inputStr.charAt(i) + "");
                userCode.add(digit);
            } catch (NumberFormatException e) {
                throw new InvalidCodeException("Only digit allowed! Please enter 4 digits!");
            }
        }

        return userCode;

    }

    public boolean isValid(ArrayList<Integer> arrayList) throws CodeLengthException, EmptyInputException, DuplicateDigitException {
        boolean isInputValid = false;
        if (arrayList.size() == 0) {
            throw new EmptyInputException("Empty input! Please enter 4 digits!");
        } else if (arrayList.size() > 4) {
            throw new CodeLengthException("Input is too long! Please enter 4 digits!");
        } else if (arrayList.size() < 4) {
            throw new CodeLengthException("Input is too short! Please enter 4 digits!");
        } else {
            boolean isDuplicate = false;
            for (Integer i : arrayList) {
                if (arrayList.subList(0, arrayList.indexOf(i)).contains(i)) {
                    isDuplicate = true;
                }
            }
            if (isDuplicate) {
                throw new DuplicateDigitException("No duplicate digit allowed! Please enter 4 digits!");
            } else {
                isInputValid = true;
            }
        }

        return isInputValid;
    }
}
