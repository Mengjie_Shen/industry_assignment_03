import java.util.ArrayList;


/**
 * Created by Mengjie
 * Date : 2017/12/16
 * Time : 16:23
 **/
public class GameProcessor {

    private ArrayList<Integer> code;
    private ArrayList<Integer> guess;

    public GameProcessor(ArrayList<Integer> code, ArrayList<Integer> guess) {
        this.code = code;
        this.guess = guess;

    }

    public int getBull() {
        int bullCount = 0;
        for (int i = 0; i < guess.size(); i++) {
            if (guess.get(i) == code.get(i)) {
                bullCount++;
            }
        }
        return bullCount;
    }

    public int getCow() {
        int cowCount = 0;
        for (int i = 0; i < guess.size(); i++) {
            if (guess.get(i) != code.get(i) && code.contains(guess.get(i))) {
                cowCount++;
            }
        }
        return cowCount;
    }

    @Override
    public String toString() {
        if (this.getBull() != 1) {
            if (this.getCow() != 1) {
                return "Result: " + this.getBull() + " bulls and " + this.getCow() + " cows";
            } else {
                return "Result: " + this.getBull() + " bulls and " + this.getCow() + " cow";
            }
        } else {
            if (this.getCow() != 1) {
                return "Result: " + this.getBull() + " bull and " + this.getCow() + " cows";
            } else {
                return "Result: " + this.getBull() + " bull and " + this.getCow() + " cow";
            }
        }
    }
}
