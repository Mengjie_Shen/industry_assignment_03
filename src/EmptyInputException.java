/**
 * Created by Mengjie
 * Date : 2017/12/16
 * Time : 18:35
 **/
public class EmptyInputException extends Exception {
    public EmptyInputException(String s) {
        super(s);
    }
}
